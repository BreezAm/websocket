package com.itncsc.websocket.core.websocket;

import com.itncsc.websocket.domain.Message;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicInteger;

@Component
@ServerEndpoint(value = "/ws/{userId}/{target}")
public class WebSocketService {
    //用于保存连接的用户信息
    private static ConcurrentHashMap<String, Session> SESSION = new ConcurrentHashMap<>();
    //原子递增递减，用于统计在线用户数
    private static AtomicInteger count = new AtomicInteger();
    //消息队列，用于保存待发送的信息
    private Queue<String> queue = new LinkedBlockingDeque<>();

    @OnOpen
    public void onOpen(Session session, @PathParam("userId") String userId) {
        if (!SESSION.containsKey(userId)) {
            SESSION.put(userId, session);
            count.incrementAndGet();
        }
    }

    @OnClose
    public void onClose(@PathParam("userId") String userId) {
        SESSION.remove(userId);
        count.decrementAndGet();
    }

    @OnMessage
    public void onMessage(String message, @PathParam("userId") String userId, @PathParam("target") String target) throws IOException {
        queue.add(message);
        Session s = SESSION.get(target);
        if (s == null) {
            Session b = SESSION.get(userId);
            b.getBasicRemote().sendText("对方不在线");
        } else {
            for (int i = 0; i < queue.size(); i++) {
                String msg = queue.poll();
                Message m = new Message();
                m.setUserId(userId);
                s.getBasicRemote().sendText(msg);
            }
        }
    }
    @OnError
    public void onError(Throwable error, @PathParam("userId") String userId) {
        SESSION.remove(userId);
        count.decrementAndGet();
        error.printStackTrace();
    }
}
