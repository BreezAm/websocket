package com.itncsc.websocket.domain;

import lombok.Data;

@Data
public class Message {
    private String userId;
    private String msg;
}
