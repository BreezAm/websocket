![输入图片说明](doc/img/index.png)

# websocket

#### 介绍
 该项目是一个及时聊天案例，实现简单的点对点聊天。

| ![输入图片说明](doc/img/c1.png)  |  ![输入图片说明](doc/img/c2.png) |
|---|---|


### 💖线上预览：[http://chat.breez.work](http://chat.breez.work/)
 

#### 技术栈
- Spring Boot
- Spring WebSocket
- Nuxt
- Vue
- Element UI



#### 安装教程
##### 后端安装教程
1.将项目克隆到本地

```shell
git clone git@gitee.com:BreezAm/websocket.git
```
2、用IDEA或者其他编辑器打开项目

3.运行`WebsocketApplication`

4。默认端口号是`3688`，可以自行修改

```
server.port=3688
```


#### 前端安装教程

1. 打开克隆下来的项目，进入site文件夹。
2.  如果修改过后台端口号，需要在pages/chat页面修改以下代码为自己的信息

```
this.socket = new WebSocket("ws://localhost:3688/ws/" + this.user.userId + "/" + this.user.targetId);
```

3.  依次执行以下命令

```
cnpm install

cnpm run dev
```
4.浏览器打开

 [http://localhost:3000](http://localhost:3000)

#### 使用说明

1.  输入我的喵喵号和对方喵喵号，喵喵号没有特殊含义，只作为一个唯一ID。

![输入图片说明](doc/imgimage.png)
2.  点击聊一下可进入聊天页面

 

3.  只要对方相互输入ID即可连接聊天，和QQ一样的。

#### 实现讲解
CSDN：[SpringBoot整合websocket实现及时通信聊天](https://blog.csdn.net/qq_43073558/article/details/124616426?spm=1001.2014.3001.5502)
#### 参考文献

 - MDN：[WebSocket](https://developer.mozilla.org/zh-CN/docs/Web/API/WebSocket/WebSocket)
- Nuxt：[https://nuxtjs.org](https://nuxtjs.org/)
- Vue：[https://cn.vuejs.org](https://cn.vuejs.org/)
- 百度百科：[及时通信](https://baike.baidu.com/item/%E5%8D%B3%E6%97%B6%E9%80%9A%E8%AE%AF/111144)

 
